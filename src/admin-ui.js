import Ripple from 'vue-ripple-directive'
import VueTouch from 'vue-touch'

import SideMenuContainer from '@/components/SideMenuContainer.vue'
import SideMenuGroup from '@/components/SideMenuGroup.vue'
import SideMenuTopLevelItem from '@/components/SideMenuTopLevelItem.vue'
import Spacer from '@/components/Spacer.vue'

export default function install (Vue, options) {
  if (!options.hasOwnProperty('router')) {
    throw new Error('Vue Router is required!')
  }
  Vue.directive('ripple', Ripple)
  Vue.use(VueTouch)

  Vue.component('er-side-menu-container', SideMenuContainer)
  Vue.component('er-side-menu-group', SideMenuGroup)
  Vue.component('er-side-menu-top-level-item', SideMenuTopLevelItem)
  Vue.component('er-spacer', Spacer)
}

export {
  SideMenuContainer,
  SideMenuGroup
}
